// It is important that an arrow function isn't used here so a new scope is created.
const MyModule = (function() {
  let privateVariable = 10;

  const privateMethod = () => {
    console.log('Inside a private method!');
    privateVariable++;
  }

  const methodToExpose = () => {
    console.log(
      'This is a method I want to expose!',
      'The privateVariable is',
      privateVariable
    );
  }

  const otherMethodIWantToExpose = () => {
    privateMethod();
  }

  return {
      methodOne: methodToExpose,
      methodTwo: otherMethodIWantToExpose
  };
})(); // Note the braces here causing immediate execution of the function.

MyModule.methodOne();        // Output: This is a method I want to expose! The privateVariable is 10
MyModule.methodTwo();       // Output: Inside a private method!
MyModule.methodOne();        // Output: This is a method I want to expose! The privateVariable is 11
console.log('Trying to access methodToExpose directly', MyModule.methodToExpose ); // undefined
console.log('Trying to access privateVariable directly', MyModule.privateVariable ); // undefined

// Export our Exposer object so it can be required via other modules/scripts.
module.exports = MyModule;
