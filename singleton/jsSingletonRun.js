const MySingleton = require('./jsSingleton');

console.log(
  `The first reference of singleton has been imported, the myVar value is ${MySingleton.getMyVar()}`
);

const MyOtherSingletonInstance = require('./jsSingleton');

console.log(
  `The second reference of singleton has been imported, the myVar value is ${MyOtherSingletonInstance.getMyVar()}`
);

MySingleton.setMyVar('No longer the default!');

console.log(
  `The first reference of singleton myVar value is ${MySingleton.getMyVar()}`
);
console.log(
  `The second reference of singleton myVar value is ${MyOtherSingletonInstance.getMyVar()}`
);
