const Subject = require('./subject');
class MyInterestingEmitClass {
  constructor() {
    this.Subject = new Subject();
    this.subjects = {
      'myInterestingAction' : []
    };
  }
  registerEventObserver( eventKey, observer ) {
    this.subjects[eventKey].push(observer);
    this.Subject.subscribeObserver(observer);
  };

  doMyInterestingAction( ) {
    console.log( 'My interesting action has been fired!' );
    // Now we have to notify all the observers manually for this action.
    this.subjects['myInterestingAction'].forEach( observer => this.Subject.notifyObserver(observer) );
  }
}

module.exports = MyInterestingEmitClass;
