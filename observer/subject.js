const Subject = function() {
  this.observers = [];

  return {
    subscribeObserver: (observer) => {
      this.observers.push(observer);
    },
    unsubscribeObserver: (observer) => {
      var index = this.observers.indexOf(observer);
      if(index > -1) {
        this.observers.splice(index, 1);
      }
    },
    notifyObserver: (observer) => {
      var index = this.observers.indexOf(observer);
      if(index > -1) {
        this.observers[index].notify(index);
      }
    },
    notifyAllObservers: () => {
      for(var i = 0; i < this.observers.length; i++){
        this.observers[i].notify(i);
      };
    }
  };
};
module.exports = Subject;
