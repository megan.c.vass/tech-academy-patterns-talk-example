const Observer = function( clb ) {
  const callback = clb  || function(){};
  return {
    notify: function(index) {
      callback(index)
      console.log(`Observer ${index} is notified!`);
    }
  }
}
module.exports = Observer;
