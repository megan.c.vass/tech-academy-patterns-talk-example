const EventPubSub = require('./eventPubSub');
class MyPublisher {
  constructor() {
    this.eventSubs = [
      EventPubSub.subscribe('myPublisher#publisherEvent', this.publishedEventHandler )
    ];
  }

  publishedEventHandler( data ) {
    console.log(`Event publish has been handled! `, data);
  }

  unsubscribeFromPublishers( ) {
    this.eventSubs.forEach( (sub,index) => {
       sub.remove();
       this.eventSubs.splice(index, 1);
    });
  }
}
module.exports = MyPublisher;
