const EventPubSub = require('./eventPubSub');
class MyPublisher {
  constructor( ) { }
  publishEvent( ) {
    EventPubSub.publish('myPublisher#publisherEvent', {
      info: 'I was passed data by the event publisher'
    });
  }
}
module.exports = MyPublisher;
