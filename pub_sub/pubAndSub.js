const myPublisher = require('./myPublisher');
const mySubscriber = require('./mySubscriber');

const publisherInstance = new myPublisher();
const subscriberInstance = new mySubscriber();

publisherInstance.publishEvent();

subscriberInstance.unsubscribeFromPublishers();

publisherInstance.publishEvent();
