(function () {
    'use strict';

    /***************************************************************
     * Helper functions for older browsers
     ***************************************************************/
    if (!Object.hasOwnProperty('create')) {
        Object.create = function (parentObj) {
            function tmpObj() {}
            tmpObj.prototype = parentObj;
            return new tmpObj();
        };
    }
    if (!Object.hasOwnProperty('defineProperties')) {
        Object.defineProperties = function (obj, props) {
            for (var prop in props) {
                Object.defineProperty(obj, prop, props[prop]);
            }
        };
    }
    /*************************************************************/

    const Vehicle = {
      name: 'Vehicle',
      model: '',
      type: 'Vehicle',
      getName: function() {
        return this.name;
      },
      setName( name ) {
        this.name = name;
      },
      getModel: function () {
        return this.model;
      },
      setModel: function(model) {
        this.model = model;
      },
      getType: function () {
        return this.type;
      }
    };

    const Truck = Object.create(Vehicle, {
        type: {value: 'Truck'}
    });

    const Van = Object.create(Vehicle, {
        type: {value: 'Van'}
    });

    const F150 = Object.create(Truck, {
        model: {value: 'F-150', writable: true}
    });

    const Sprinter = Object.create(Van, {
        model: {value: 'Sprinter'}
    });

    console.log( `${F150.getName()} is a ${F150.getType()} and the model is ${F150.getModel()}` );
    console.log( `${Sprinter.getName()} is a ${Sprinter.getType()} and the model is ${Sprinter.getModel()}` );

    F150.setModel('RAM 1500');

    console.log( `${F150.getName()} is a ${F150.getType()} and the model is ${F150.getModel()}` );
    console.log( `${Sprinter.getName()} is a ${Sprinter.getType()} and the model is ${Sprinter.getModel()}` );
})();
