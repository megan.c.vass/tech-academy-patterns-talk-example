/**
 * Create a new constructor function, whose prototype is the parent object's prototype.
 * Set the child's prototype to the newly created constructor function.
 **/
const childInheritFromParent = function(childObj, parentObj) {
    var tmpObj = function () {}
    tmpObj.prototype = parentObj.prototype;
    childObj.prototype = new tmpObj();
    childObj.prototype.constructor = childObj;
};

// base human object
var Vehicle = function () {};
// inhertiable attributes / methods
Vehicle.prototype = {
    name: 'Vehicle',
    model: '',
    type: 'Vehicle',
    getName: function() {
      return this.name;
    },
    setName( name ) {
      this.name = name;
    },
    getModel: function () {
      return this.model;
    },
    setModel: function(model) {
      this.model = model;
    },
    getType: function () {
      return this.type;
    }
};

// Truck
var Truck = function () {
    this.model = 'FORD F-150';
    this.type = 'Truck';
};
// inherits Vehicle
childInheritFromParent(Truck, Vehicle);

// Van
var Van = function () {
  this.model = 'Sprinter';
  this.type = 'Van';
};
// inherits Vehicle
childInheritFromParent(Van, Vehicle);


// try out our Pseudo classical inheritance
var MyTruck = new Truck();
var MyVan = new Van();

console.log( `${MyTruck.getName()} is a ${MyTruck.getType()} and the model is ${MyTruck.getModel()}` );
console.log( `${MyVan.getName()} is a ${MyVan.getType()} and the model is ${MyVan.getModel()}` );

MyTruck.setModel('RAM 1500');

console.log( `${MyTruck.getName()} is a ${MyTruck.getType()} and the model is ${MyTruck.getModel()}` );
console.log( `${MyVan.getName()} is a ${MyVan.getType()} and the model is ${MyVan.getModel()}` );
