const vehicle = function(attrs) {
    const _privateObj = {
        hasEngine: true
    };
    let that = {};

    that.name = attrs.name || null;
    that.engineSize = attrs.engineSize || null;
    that.hasEngine = function () {
        console.log('This ' + that.name + ' has an engine: ' + _privateObj.hasEngine);
    };

    return that;
}

const motorbike = function () {

    // private
    const _privateObj = {
        numWheels: 2
    },

    // inherit
    that = vehicle({
        name: 'Motorbike',
        engineSize: 'Small'
    });

    // public
    that.totalNumWheels = function () {
        console.log('This Motorbike has ' + _privateObj.numWheels + ' wheels');
    };

    that.increaseWheels = function () {
        _privateObj.numWheels++;
    };

    return that;

};

const boat = function () {

    // inherit
    that = vehicle({
        name: 'Boat',
        engineSize: 'Large'
    });

    return that;

};

const myBoat = boat();
myBoat.hasEngine(); // This Boat has an engine: true
console.log(myBoat.engineSize); // Large

const myMotorbike = motorbike();
myMotorbike.hasEngine(); // This Motorbike has an engine: true
myMotorbike.increaseWheels();
myMotorbike.totalNumWheels(); // This Motorbike has 3 wheels
console.log(myMotorbike.engineSize); // Small

const myMotorbike2 = motorbike();
myMotorbike2.totalNumWheels(); // This Motorbike has 2 wheels

console.log( `Trying to access private numWheels value : ${myMotorbike._privateObj}` ); // undefined
console.log( `Trying to access myBoats wheels : ${myBoat.totalNumWheels}` ); // undefined
